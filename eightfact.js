// find factorial of the number
const number = 5;
let fact = 1;
const factorial = (number)=>{
 for(let i=number; i>0; i--){
    fact = i * fact;
 }
 return fact;
}
console.log(factorial(number));