// Find max & Min value
const arrNumber = [34,1,9,7,15,87,65,558];
const maxNumber = Math.max(...arrNumber);
const minNumber = Math.min(...arrNumber);
console.log(maxNumber,minNumber);

let minVal = arrNumber.reduce((prev,curr,index,arr)=>{
   return prev<curr? prev:curr;
});
console.log(minVal);

let maxVal = arrNumber.reduce((prev,curr,index,arr)=>{
    return prev>curr? prev:curr;
 });
 console.log(maxVal);