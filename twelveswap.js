// swapping using 3rd variable
let a=7;
let b=8;
let temp = a;
 a = b;
 b = temp;
console.log(a,b);

// swapping without 3rd variable
let a1 = 10;
let b1 = 20;
a1 = a1 + b1; //30
b1 = a1 - b1; // 10
a1 = a1 - b1; // 20
console.log(a1,b1);

let val1 = 20;
let val2 = 40; let val3 =45;
 [val1,val2,val3] =[val3,val2,val1];
console.log("val1 is ",val1,"val2 is",val2, val3);

