// filter() method returns matched values in array fom collection
// find() method retuens first matched value

const empArr = [
    { name:"saurabh",age:22},
    { name:"tushad behram",age:37},
    { name:"ratan",age:32},
    { name:"surabhi",age:29},
    { name:"sameer",age:57},
    { name:"raj",age:82},
    { name:"tushar",age:22},
];

let byFilter = empArr.filter((item,ind,arr)=>{
    return  item.age >30;
})
console.log(byFilter);

let byFind = empArr.find((item)=>{
    return  item.age >30;
})
console.log(byFind);